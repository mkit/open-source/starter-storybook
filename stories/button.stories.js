import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { boolean, text } from '@storybook/addon-knobs';
import withPropsCombinations from 'react-storybook-addon-props-combinations'; // https://github.com/evgenykochetkov/react-storybook-addon-props-combinations
import Button from '../src/components/Button/Button';

const stories = storiesOf('Button', module);

stories
  .add('Basic', () => <Button>Basic</Button>)

  .add('with Actions', () => <Button onClick={action('button-with-actions')}>Actions</Button>)

  .add('with Knobs', () => (
    <Button type="button" disabled={boolean('Disabled', false)}>
      {text('Label', 'Knobs')}
    </Button>
  ))

  .add('with Notes', () => <Button>Notes</Button>, {
    notes: 'A very simple component'
  })

  .add('with Console', () => <Button onClick={() => console.log('Data:', 1, 3, 4)}>Console</Button>) // eslint-disable-line

  .add(
    'with Props Combinations',
    withPropsCombinations('button', {
      disabled: [false, true],
      onClick: [action('clicked')],
      children: ['hello world', <b>some elements</b>]
    })
  );
